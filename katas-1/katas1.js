function oneThroughTwenty() {
  for(let i = 1;i <= 20; i++){
    console.log(i) 
  }  
}

oneThroughTwenty()

//call function oneThroughTwenty

function evensToTwenty() {
  for(let i = 1;i <= 20; i++){
    if(i % 2 === 0){
      console.log(i)
    }
  } 
}

evensToTwenty()

//call function evensToTwenty

function oddsToTwenty() {
  for(let i = 1;i <= 20; i++){
    if(i % 2 !== 0){
      console.log(i)
    }
  } 
}

oddsToTwenty()


//call function oddsToTwenty

function multiplesOfFive() {
  for(let i = 1;i <= 100; i++){
    if(i % 5 === 0){
      console.log(i)
    }
  } 
}

multiplesOfFive()

//call function multiplesOfFive

function squareNumbers() {
  for(let i = 1; i <= 100; i++){
    for(let a = 0; a <= i; a++){
      if(a * a === i){
        console.log(i)
      }
    }
  }
}

squareNumbers()
//call function squareNumbers

function countingBackwards() {
  for(let i = 20;i > 0; i--){
    console.log(i) 
  }  
}

countingBackwards()
//call function countingBackwards

function evenNumbersBackwards() {
  for(let i = 20;i > 0; i--){
    if(i % 2 === 0){
      console.log(i)
    }
  } 
}

evenNumbersBackwards()

//call function evenNumbersBackwards

function oddNumbersBackwards() {
  for(let i = 20;i > 0; i--){
    if(i % 2 !== 0){
      console.log(i)
    }
  } 
}

oddNumbersBackwards()

//call function oddNumbersBackwards

function multiplesOfFiveBackwards() {
  for(let i = 100;i > 0; i--){
    if(i % 5 === 0){
      console.log(i)
    }
  } 
}

multiplesOfFiveBackwards()
//call function multiplesOfFiveBackwards

function squareNumbersBackwards(){
  for(let i = 100; i >= 1; i--){
    for(let a = 0; a <= i; a++){
      if(a * a === i){
        console.log(i)
      }
    }
  }
}

squareNumbersBackwards()
//call function squareNumbersBackwards