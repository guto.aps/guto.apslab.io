// comece a criar a sua função add na linha abaixo
function add(n1, n2){
  return n1 + n2
}


// descomente a linha seguinte para testar sua função
console.assert(add(3, 5) === 8, 'A função add não está funcionando como esperado');



// comece a criar a sua função multiply na linha abaixo
function multiply(n1, n2){
  let soma = 0
  for(let i = 0; i < n2; i = add(i,1)){
    soma = add(soma, n1)
  }
  return(soma)
}

// descomente a linha seguinte para testar sua função
console.assert(multiply(4, 6) === 24, 'A função multiply não está funcionando como esperado');


// comece a criar a sua função power na linha abaixo

function power(n1, n2){
  let multiplicacao = n1
  for(let i = 1; i < n2; i = add(i,1)){
    multiplicacao = multiply(multiplicacao, n1)
  }
  return(multiplicacao)
}


// descomente a linha seguinte para testar sua função
console.assert(power(3, 4) === 81, 'A função power não está funcionando como esperado');


// comece a criar a sua função factorial na linha abaixo

function sub(n1, n2){
  return n1 - n2
}

function factorial(n1){
  let multiplicacao = n1
  for(let i = sub(n1,1); i > 0; i = sub(i, 1)){
    multiplicacao = multiply(multiplicacao, i) 
  }
  return(multiplicacao)
}

// descomente a linha seguinte para testar sua função
console.assert(factorial(5) === 120, 'A função factorial não está funcionando como esperado');


/**
 * BONUS (aviso: o grau de dificuldade é bem maior !!
 */

// crie a função fibonacci

function fibonacci(n1){
  let fibo = []
  for(let i = 0; i < n1; i = add(i,1)){
    if(i === 0 || i === 1){
      fibo.push(i)
    }
    if(i > 1){
      fibo.push(add(fibo[sub(fibo.length, 1)], fibo[sub(fibo.length, 2)]))
    }
  }
  return(fibo[sub(fibo.length,1)])
}


// descomente a linha seguinte para testar sua função
console.assert(fibonacci(8) === 13, 'A função fibonacci não está funcionando como esperado');
